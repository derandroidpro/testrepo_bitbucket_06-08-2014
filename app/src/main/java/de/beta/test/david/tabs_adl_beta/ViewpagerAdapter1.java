package de.beta.test.david.tabs_adl_beta;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import de.beta.test.david.tabs_adl_beta.tab_fragmente.Fragment1Tab;
import de.beta.test.david.tabs_adl_beta.tab_fragmente.Fragment2Tab;

public class ViewpagerAdapter1 extends FragmentPagerAdapter{


    String [] tabtitel = {"erster Tab", "zweiter Tab"};

    public ViewpagerAdapter1(FragmentManager manager) {
        super(manager);
    }

    @Override
    public Fragment getItem(int position) {

        switch (position){

            case 0: return new Fragment1Tab();
            case 1: return new Fragment2Tab();
        }

        return null;
    }

    @Override
    public int getCount() {
        return 2;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return tabtitel[position];
    }
}
