package de.beta.test.david.tabs_adl_beta;

import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import java.util.ArrayList;

import de.beta.test.david.tabs_adl_beta.tab_fragmente.Fragment1Tab;
import de.beta.test.david.tabs_adl_beta.tab_fragmente.Fragment2Tab;

public class MainActivity extends AppCompatActivity {

    Toolbar toolbar1;

    TabLayout tabLayout1;

    ViewPager viewPager1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // TOOLBAR

        toolbar1 = (Toolbar) findViewById(R.id.toolbar1);
        setSupportActionBar(toolbar1);

        // VIEW PAGER

        viewPager1 = (ViewPager) findViewById(R.id.viewpager1);
        final ViewpagerAdapter1 viewpagerAdapter1 = new ViewpagerAdapter1(getSupportFragmentManager());
        viewPager1.setAdapter(viewpagerAdapter1);

        // TAB LAYOUT

        tabLayout1 = (TabLayout) findViewById(R.id.tablayout1);
        tabLayout1.setupWithViewPager(viewPager1);
        tabLayout1.setTabGravity(TabLayout.GRAVITY_FILL);

        tabLayout1.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                Log.d("onTabselected", "onTabSelected ausgeführt, TabLayout");
                viewPager1.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

    }

}
